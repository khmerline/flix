class AddAttachmentImageFileNameToMovies < ActiveRecord::Migration
  def self.up
    change_table :movies do |t|
      t.attachment :image_file_name
    end
  end

  def self.down
    remove_attachment :movies, :image_file_name
  end
end
